import supertest from 'supertest';
import dotenv from 'dotenv';

dotenv.config();
const request = supertest('http://localhost:8080/api');

test('Deve responder na porta 8080', () => {
  /* acessar a url http://localhost:8080/api
  * verificar se a resposta foi 404, se houve resposta
  * significa que o servidor esta respondendo corretamente
  */
  request.get('/').then((res) => {
    expect(res.status).toBe(404);
  });
});
