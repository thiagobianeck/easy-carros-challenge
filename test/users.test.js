import mongoose from 'mongoose';
import dotenv from 'dotenv';
import bcrypt from 'bcryptjs';
import supertest from 'supertest';
import Users from '../src/models/users';
import app from '../index';

dotenv.config();
const mongoDB = process.env.URL_MONGODB_TEST;

mongoose.connect(mongoDB);

test('Deve configurar NODE_ENV para test', () => {
  const nodeEnv = process.env.NODE_ENV;
  expect(nodeEnv).toEqual('test');
});

describe('Users TestModel', () => {
  beforeAll(async () => {
    await Users.remove({});
  });

  afterEach(async () => {
    await Users.remove({});
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  test('tem um modulo', () => {
    expect(Users).toBeDefined();
  });

  describe('get Users', () => {
    test('Deve listar um usuario', async () => {
      const user = new Users({
        role: 'admin',
        name: 'Thiago',
        email: 'thiagobianeck@gmail.com',
        password: 'teste123',
      });
      await user.save();

      const foundUser = await Users.findOne({ name: 'Thiago' });
      const expected = 'Thiago';
      const actual = foundUser.name;
      expect(actual).toEqual(expected);
    });
  });

  describe('save Users', () => {
    test('Deve salvar um usuario', async () => {
      const user = new Users({
        role: 'admin',
        name: 'Thiago',
        email: 'thiagobianeck@gmail.com',
        password: 'teste123',
      });
      const savedUser = await user.save();
      const expected = 'Thiago';
      const actual = savedUser.name;
      expect(actual).toEqual(expected);
    });
  });

  describe('update Users', () => {
    test('Deve atualizar um usuario', async () => {
      const user = new Users({
        role: 'admin',
        name: 'Thiago',
        email: 'thiagobianeck@gmail.com',
        password: 'teste123',
      });
      await user.save();

      user.name = 'Leonardo';
      const updatedUser = await user.save();

      const expected = 'Leonardo';
      const actual = updatedUser.name;
      expect(actual).toEqual(expected);
    });
  });

  describe('login Users', () => {
    test('Deve encriptar a senha', async () => {
      const user = new Users({
        role: 'admin',
        name: 'Thiago',
        email: 'thiagobianeck@gmail.com',
        password: await bcrypt.hash('teste123', 10),
      });
      await user.save();
      const actual = user.password;
      expect(actual).not.toEqual('teste123');
    });
  });
});

describe('Testes de rotas', () => {
  const request = supertest;

  test('Deve inserir usuario com token de admin com sucesso', async () => {
    const admin = await request(app).post('/api/users/login')
      .send({ email: 'thiagobianeck@gmail.com', password: 'Bimorethi27' })
      .then((res) => res);
    const token = admin.body.tokenReturn;

    return request(app).post('/api/users/add')
      .set({ token, Accept: 'application/json' })
      .send({
        role: 'user', name: `Usuario ${Date.now()}`, email: `${Date.now()}@gmail.com`, password: 'lukesky',
      })
      .then((res) => {
        expect(res.status).toBe(201);
      });
  });

  test('Deve logar usuario no sistema com sucesso e retornar token', async () => {
    const admin = await request(app).post('/api/users/login')
      .send({ email: 'thiagobianeck@gmail.com', password: 'Bimorethi27' })
      .then((res) => res);
    const tokenAdmin = admin.body.tokenReturn;

    const userName = `Usuario ${Date.now()}`;
    const userEmail = `${Date.now()}@gmail.com`;
    const userPassword = 'lukesky';

    await request(app).post('/api/users/add')
      .set({ token: tokenAdmin, Accept: 'application/json' })
      .send({
        role: 'user', name: userName, email: userEmail, password: userPassword,
      })
      .then((res) => res);

    await request(app).post('/api/users/login')
      .send({ email: userEmail, password: userPassword })
      .then((res) => {
        expect(res.body.tokenReturn).toBeDefined();
      });
  });
});
