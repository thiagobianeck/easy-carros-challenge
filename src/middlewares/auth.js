import tokenService from '../services/token';

export default {
  verifyUser: async (req, res, next) => {
    if (!req.headers.token) {
      return res.status(404).send({ message: 'Sem Token' });
    }
    const response = await tokenService.decode(req.headers.token);
    if (response.role === 'admin' || response.role === 'user') {
      next();
      return false;
    }
    return res.status(403).send({ message: 'Não Autorizado' });
  },
  verifyAdmin: async (req, res, next) => {
    if (!req.headers.token) {
      return res.status(404).send({ message: 'Sem Token' });
    }
    const response = await tokenService.decode(req.headers.token);
    if (response.role === 'admin') {
      next();
      return false;
    }
    return res.status(403).send({ message: 'Não Autorizado' });
  },
};
