export default {
  response500: (res) => {
    res.status(500).send({ message: 'Ocorreu um erro :(' });
  },
  response404: (res) => {
    res.status(404).send({ message: 'O Registro não existe :(' });
  },
};
