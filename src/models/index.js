import Location from './location';
import Partners from './partners';
import Users from './users';

export default {
  Location,
  Partners,
  Users,
};
