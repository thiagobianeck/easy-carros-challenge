import mongoose, { Schema } from 'mongoose';

const usersSchema = new Schema({
  role: { type: String, enum: ['admin', 'user'], required: true },
  name: {
    type: String, maxLength: 50, unique: true, required: true,
  },
  email: {
    type: String, maxLength: 50, unique: true, required: true,
  },
  password: { type: String, maxLength: 64, required: true },
});

const Users = mongoose.model('users', usersSchema);

export default Users;
