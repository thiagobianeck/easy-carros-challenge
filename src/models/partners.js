import mongoose, { Schema } from 'mongoose';

const partnersSchema = new Schema({
  name: {
    type: String,
    maxLength: 255,
    required: true,
  },
  location: {
    type: Schema.ObjectId,
    ref: 'location',
  },
  availableServices: {
    type: [String],
    enum: ['OIL_CHANGE', 'DRY_WASHING'],
    required: true,
  },
});

const Partners = mongoose.model('partners', partnersSchema);

export default Partners;
