import mongoose, { Schema } from 'mongoose';

const locationSchema = new Schema({
  name: {
    type: String,
    maxLength: 255,
    required: true,
  },
  address: {
    type: String,
    maxLength: 255,
    required: true,
  },
  city: {
    type: String,
    maxLength: 60,
    required: true,
  },
  state: {
    type: String,
    maxLength: 2,
    required: true,
  },
  country: {
    type: String,
    maxLength: 50,
    required: true,
  },
  lat: {
    type: Number,
    required: true,
  },
  long: {
    type: Number,
    required: true,
  },
});

const Location = mongoose.model('location', locationSchema);

export default Location;
