import * as geolib from 'geolib';
import * as googleMapsClient from '@google/maps';
import dotenv from 'dotenv';

dotenv.config();

googleMapsClient.createClient({
  key: process.env.GOOGLE_API_KEY,
  Promise,
});

export default {
  /*
  * Poderia neste caso ter feito a consulta no próprio controller
  * usando o mongodb para calcular as distâncias entre os pontos
  * usando $geoNear, mas como esta lib já me facilitava o processo,
  * resolvi testar o funcionamento.
  */
  getPartnerWithinRadius: (point, center, radius) => {
    geolib.isPointWithinRadius(point, center, radius);
  },
  getLatLongByAddress: (address) => googleMapsClient.createClient({
    key: process.env.GOOGLE_API_KEY,
    Promise,
  }).geocode({ address })
    .asPromise()
    .then((response) => {
      const { location } = response.json.results[0].geometry;
      const latitude = location.lat;
      const longitude = location.lng;
      return { latitude, longitude };
    })
    .catch((err) => {
      /* eslint no-console: ["error", { allow: ["warn", "error"] }] */
      console.error(err);
    }),
};
