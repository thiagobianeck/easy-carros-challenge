import jwt from 'jsonwebtoken';
import models from '../models';

async function checkToken(token) {
  // eslint-disable-next-line no-underscore-dangle
  let __id = null;
  try {
    const { _id } = await jwt.decode(token);
    __id = _id;
  } catch (e) {
    return false;
  }

  const user = await models.Users.findOne({ _id: __id });
  if (user) {
    const tokenC = jwt.sign({ _id: __id }, 'thisisasecretkeyforeasycarroschallenge', { expiresIn: '1d' });
    return { tokenC, role: user.role };
  }
  return false;
}

export default {
  encode: async (_id) => {
    const tokenE = jwt.sign({ _id }, 'thisisasecretkeyforeasycarroschallenge', { expiresIn: '1d' });
    return tokenE;
  },
  decode: async (token) => {
    try {
      const { _id } = await jwt.verify(token, 'thisisasecretkeyforeasycarroschallenge');
      const user = await models.Users.findOne({ _id });
      if (user) {
        return user;
      }
      return false;
    } catch (e) {
      return checkToken(token);
    }
  },
};
