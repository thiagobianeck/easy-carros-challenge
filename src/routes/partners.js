import routerx from 'express-promise-router';
import auth from '../middlewares/auth';
import partnersController from '../controllers/PartnersController';

const router = routerx();

router.post('/add', auth.verifyUser, partnersController.add);
router.post('/getservices', auth.verifyUser, partnersController.getServicesByLatLong);
router.post('/getservicesbyaddress', auth.verifyUser, partnersController.getServicesByAddress);
router.get('/query', auth.verifyUser, partnersController.query);
router.get('/list', auth.verifyUser, partnersController.list);
router.put('/update', auth.verifyAdmin, partnersController.update);
router.delete('/remove', auth.verifyAdmin, partnersController.remove);

export default router;
