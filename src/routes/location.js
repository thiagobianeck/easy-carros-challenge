import routerx from 'express-promise-router';
import auth from '../middlewares/auth';
import locationController from '../controllers/LocationController';

const router = routerx();

router.post('/add', auth.verifyUser, locationController.add);
router.get('/query', auth.verifyUser, locationController.query);
router.get('/list', auth.verifyUser, locationController.list);
router.put('/update', auth.verifyAdmin, locationController.update);
router.delete('/remove', auth.verifyAdmin, locationController.remove);

export default router;
