import routerx from 'express-promise-router';
import locationRouter from './location';
import partnersRouter from './partners';
import usersRouter from './users';

const router = routerx();

router.use('/location', locationRouter);
router.use('/partners', partnersRouter);
router.use('/users', usersRouter);

export default router;
