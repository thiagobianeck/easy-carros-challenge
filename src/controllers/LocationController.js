import models from '../models';
import responses from '../inc/responses';

export default {

  add: async (req, res) => {
    try {
      const reg = await models.Location.create(req.body);
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
    }
  },
  query: async (req, res, next) => {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const reg = await models.Location.findOne({ _id: req.query._id });
      if (!reg) {
        res.status(404).send({
          mensagem: 'O registro não existe',
        });
      } else {
        res.status(200).json(reg);
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  list: async (req, res, next) => {
    try {
      const { valor } = req.query;
      const reg = await models.Location.find(
        {
          $or: [
            { name: new RegExp(valor, 'i') },
            { address: new RegExp(valor, 'i') },
            { city: new RegExp(valor, 'i') },
          ],
        },
      ).sort({ name: 'ascending' });
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  update: async (req, res, next) => {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const regOld = await models.Location.findById({ _id: req.body._id });
      if (regOld) {
        const reg = await models.Location.findByIdAndUpdate(
          // eslint-disable-next-line no-underscore-dangle
          { _id: req.body._id },
          {
            name: req.body.name ? req.body.name : regOld.name,
            address: req.body.address ? req.body.address : regOld.address,
            city: req.body.city ? req.body.city : regOld.city,
            state: req.body.state ? req.body.state : regOld.state,
            country: req.body.country ? req.body.country : regOld.country,
            lat: req.body.lat ? req.body.lat : regOld.lat,
            long: req.body.long ? req.body.long : regOld.long,
          },
        );
        res.status(200).json(reg);
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  remove: async (req, res, next) => {
    try {
      const reg = await models.Location.findByIdAndDelete(
        // eslint-disable-next-line no-underscore-dangle
        { _id: req.body._id },
      );
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
};
