import bcrypt from 'bcryptjs';
import models from '../models';
import token from '../services/token';
import responses from '../inc/responses';

export default {
  add: async (req, res, next) => {
    try {
      req.body.password = await bcrypt.hash(req.body.password, 10);
      const reg = await models.Users.create(req.body);
      res.status(201).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  query: async (req, res, next) => {
    try {
      const reg = await models.Users
      // eslint-disable-next-line no-underscore-dangle
        .findOne({ _id: req.query._id });
      if (!reg) {
        responses.response404(res);
      } else {
        res.status(200).json(reg);
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  list: async (req, res, next) => {
    try {
      const { valor } = req.query;
      const reg = await models.Users
        .find(
          {
            $or: [
              { name: new RegExp(valor, 'i') },
              { email: new RegExp(valor, 'i') },
            ],
          },
        )
        .sort({ name: 'ascending' });
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  update: async (req, res, next) => {
    try {
      const pass = req.body.password;
      // eslint-disable-next-line no-underscore-dangle
      const reg0 = await models.Users.findOne({ _id: req.body._id });
      if (pass !== reg0.password) {
        req.body.password = await bcrypt.hash(req.body.password, 10);
      }
      const reg = await models.Users.findByIdAndUpdate(
        // eslint-disable-next-line no-underscore-dangle
        { _id: req.body._id },
        {
          role: req.body.role ? req.body.role : reg0.role,
          name: req.body.name ? req.body.name : reg0.name,
          email: req.body.email ? req.body.email : reg0.email,
          password: req.body.password ? req.body.password : reg0.password,
        },
      );
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  remove: async (req, res, next) => {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const reg = await models.Users.findByIdAndDelete({ _id: req.body._id });
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  login: async (req, res, next) => {
    try {
      const user = await models.Users.findOne({ email: req.body.email });
      if (user) {
        const match = await bcrypt.compare(req.body.password, user.password);
        if (match) {
          // eslint-disable-next-line no-underscore-dangle
          const tokenReturn = await token.encode(user._id);
          res.status(200).json({ user, tokenReturn });
        } else {
          res.status(404).send({
            message: 'Desculpe a sua senha está Incorreta, tente de novo :)',
          });
        }
      } else {
        res.status(404).send({
          message: 'Este Usuário não está registrado em nossa base! Registre-se! :)',
        });
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
};
