import models from '../models';
import responses from '../inc/responses';
import distanceService from '../services/distances';

export default {
  add: async (req, res, next) => {
    try {
      const reg = await models.Partners.create(req.body);
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  query: async (req, res, next) => {
    try {
      const reg = await models.Partners
      // eslint-disable-next-line no-underscore-dangle
        .findOne({ _id: req.query._id })
        .populate('location', { _id: 0 });
      if (!reg) {
        res.status(404).send({
          mensagem: 'O registro não existe',
        });
      } else {
        res.status(200).json(reg);
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  getServicesByLatLong: async (req, res, next) => {
    const { availableServices, lat, long } = req.body;
    try {
      const partners = await models.Partners.find({})
        .populate('location', { _id: 0 })
        .sort({ name: 'ascending' });
      const filterPartners = [];
      partners.forEach(async (partner) => {
        const services = partner.availableServices;
        let serviceFound = false;
        const latPartner = partner.location.lat;
        const longPartner = partner.location.long;
        const radius = req.body.radius ? req.body.radius : 10000;

        [...availableServices].forEach((item) => {
          if (services.indexOf(item) !== -1) serviceFound = true;
        });

        const withinRadius = distanceService.getPartnerWithinRadius(
          { latitude: latPartner, longitude: longPartner },
          { latitude: lat, longitude: long }, radius,
        );

        if (serviceFound && withinRadius) filterPartners.push(partner);
      });
      res.status(200).json(filterPartners);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  getServicesByAddress: async (req, res, next) => {
    const { availableServices, address } = req.body;
    try {
      if (address) {
        const { latitude, longitude } = await distanceService.getLatLongByAddress(address);
        const partners = await models.Partners.find({})
          .populate('location', { _id: 0 })
          .sort({ name: 'ascending' });
        const filterPartners = [];

        partners.forEach(async (partner) => {
          const services = partner.availableServices;
          let serviceFound = false;
          const latPartner = partner.location.lat;
          const longPartner = partner.location.long;
          const radius = req.body.radius ? req.body.radius : 10000;

          [...availableServices].forEach((item) => {
            if (services.indexOf(item) !== -1) serviceFound = true;
          });

          const withinRadius = distanceService.getPartnerWithinRadius(
            { latitude: latPartner, longitude: longPartner },
            { latitude, longitude }, radius,
          );

          if (serviceFound && withinRadius) filterPartners.push(partner);
        });
        res.status(200).json(filterPartners);
      } else {
        res.status(404).send({
          message: 'Endereco nao informado para busca',
        });
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  list: async (req, res, next) => {
    try {
      const { valor } = req.query;
      const reg = await models.Partners.find(
        {
          $or: [
            { name: new RegExp(valor, 'i') },
          ],
        },
      ).populate('location', { _id: 0 })
        .sort({ name: 'ascending' });
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  update: async (req, res, next) => {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const regOld = await models.Partners.findById({ _id: req.body._id });
      if (regOld) {
        const reg = await models.Partners.findByIdAndUpdate(
          // eslint-disable-next-line no-underscore-dangle
          { _id: req.body._id },
          {
            name: req.body.name ? req.body.name : regOld.name,
            availableServices: req.body.availableServices
              ? req.body.availableServices : regOld.availableServices,
          },
        );
        res.status(200).json(reg);
      }
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
  remove: async (req, res, next) => {
    try {
      const reg = await models.Partners.findByIdAndDelete(
        // eslint-disable-next-line no-underscore-dangle
        { _id: req.body._id },
      );
      res.status(200).json(reg);
    } catch (e) {
      responses.response500(res);
      next(e);
    }
  },
};
