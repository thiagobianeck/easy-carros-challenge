import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import router from './src/routes';

dotenv.config();

mongoose.Promise = global.Promise;
let dbUrl = process.env.URL_MONGODB;
if(process.env.NODE_ENV === 'test'){
  dbUrl = process.env.URL_MONGODB_TEST;
}
mongoose.connect(dbUrl, { useCreateIndex: true, useNewUrlParser: true });
const app = express();

app.use(morgan('dev'));
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', router);
app.set('port', process.env.PORT || 8080);

app.listen(app.get('port'));

export default app;
console.log('NODE_ENV', process.env.NODE_ENV);
