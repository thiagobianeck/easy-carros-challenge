# Easy Carros Challenge

Esta é uma api Rest construída para avaliar conhecimentos na linguagem javascript, utilizando NodeJs e MongoDB.  


## Como Instalar

Faça download ou clone o repositório, e instale as dependências.  
Use o comando abaixo:
  
```npm install```

- Crie no diretório raiz da aplicação o arquivo ```.env``` com as seguinte variáveis:

```
URL_MONGODB= mongodb://localhost:27017/yourdatabase
URL_MONGODB_TEST=mongodb://127.0.0.1:27017/yourdatabase_test
PORT=8080
GOOGLE_API_KEY=yourapikey
```
**Observação: Será necessário ter uma API-KEY da google para utilizar o recurso de busca por endereço.**

## Inicializar a api
Ainda é uma versão de testes, por este motivo podem haver algumas falhas ao rodar a api com o comando ```npm start```, mas todas as funcionalidades podem ser testadas utilizando o comando ```npm run dev```
Após instaladas as dependências do projeto, você poderá utilizar o comando:   
```npm run dev```

*a aplicação por padrão roda na porta 8080.*

## Rode os testes

use o comando ```npm run test```

# Resumo das rotas

A maioria das rotas tem verificação de usuário para acesso, assim que realizado o login, é gerado um token,
que deve ser utilizado no Header da requisição com a descrição: 'token'.

## Locations
Através das rotas de locations, novos endereços podem ser criados, armazenados, listados e removidos no banco.

#### Add Location

Rota utilizada para adicionar localidades, enviar no body os seguintes campos:

- name: (String)
- address: (String)
- city: (String)
- state: (String)
- country: (String)
- lat: (Number)
- long: (Number)

`POST /api/location/add` 

 #### Query Location
 
 A Rota pode ser utilizada passando com parametro na url o id da location que deverá ser encontrado.  
 Ex.: ```http://localhost:8080/api/location/query?_id=qwe342340934kjjkas```  
 
`GET /api/location/query`  

 #### List Location
 
 Essa rota faz uma listagem dos registros de locations registradas no sistema, poderá ser passado o parametro valor, que será considerado para filtragem nos campos: name, address e city.  
 Ex.: ```http://localhost:8080/api/location/valor?_id=Curitiba```
    
`GET /api/location/list`

 #### Update Location  
 
 Rota de atualização dos dados já cadastrados, passar os dados a serem atualizados.
 Obrigatório passar o id do registro a ser atualizado.
 - _id: (String)
 - name: (String)
 - address: (String)
 - city: (String)
 - state: (String)
 - country: (String)
 - lat: (Number)
 - long: (Number)
 
 Ex.: ```http://localhost:8080/api/location/update```
 
`PUT /api/location/update`
 #### Remove Location  
 
 Rota para exclusão de location.
 Obs.: passar o _id do objeto a ser excluído no body da requisição.
 
  Ex.: ```http://localhost:8080/api/location/remove```  
  
`DELETE /api/location/remove`  


## Partners (Parceiros)
Através das rotas de partners, novos endereços podem ser criados, armazenados, listados e removidos no banco.

#### Add Partner

Rota utilizada para adicionar parceiros (partners) , enviar no body os seguintes campos:

- name: (String)
- location: (id da location)  
  _usar uma location já registrada no sistema ou registrar uma antes do cadastro do partner._
- availableServices: ('OIL_CHANGE' e/ou 'DRY_WASHING'),
  _poderá ser utilizado no mínimo um dos dois valores, ou ainda os dois se achar necessário_  
  
`POST /api/partners/add` 

 #### Query Partner
 
 A Rota pode ser utilizada passando com parametro na url o id do partner (parceiro) que deverá ser encontrado.  
 Ex.: ```http://localhost:8080/api/partners/query?_id=qwe342340934kjjkas```  
 
`GET /api/partners/query`  

 #### Get Services by lat and long
 
 Essa rota faz uma listagem dos registros de partners registrados no sistema que oferecem serviços específicos, em um raio aproximado
 de 10km de distância do ponto informado.
 Os valores deverão ser passados através do body da requisição via post.  
 Os dados a serem informados são os seguintes:
 
  - availableServices: ('OIL_CHANGE' e/ou 'DRY_WASHING')
  - lat: (Number)
  - long: (Number)  
 
   
`POST /api/partners/getservices`

 #### Get Services by Address
 
 Essa rota faz uma listagem dos registros de partners registrados no sistema que oferecem serviços específicos, em um raio aproximado
 de 10km de distância do endereço informado.
 Os valores deverão ser passados através do body da requisição via post.  
 Os dados a serem informados são os seguintes:
 
  - availableServices: ('OIL_CHANGE' e/ou 'DRY_WASHING')
  - address: (String)
  
  A APi faz a verificação do endereço e faz a busca no raio de 10km, conforme as coordenadas encontradas.
 
  
`POST /api/partners/getservicesbyaddress`

 #### List Partner
 
 Essa rota faz uma listagem dos registros de partners registrados no sistema, poderá ser passado o parametro valor, que será considerado para filtragem no campo name.  
 Ex.: ```http://localhost:8080/api/partners?valor=Francisco```
    
`GET /api/partners/list`

 #### Update Partner  
 
 Rota de atualização dos dados já cadastrados, passar os dados a serem atualizados.
 Obrigatório passar o id do registro a ser atualizado.
 - _id: (String)
 - name: (String)
 - location: (id)
 - availableServices: ('OIL_CHANGE' e/ou 'DRY_WASHING')
 
 Ex.: ```http://localhost:8080/api/partners/update```
 
`PUT /api/partners/update`
 #### Remove Partner  
 
 Rota para exclusão de partner.
 Obs.: passar o _id do objeto a ser excluído no body da requisição.  
 
  Ex.: ```http://localhost:8080/api/partners/remove```  
  
`DELETE /api/partners/remove`  


## Users (Usuários)
Através das rotas de partners, novos endereços podem ser criados, armazenados, listados e removidos no banco.

#### Add User

Rota utilizada para adicionar Usuários no sistema, enviar no body os seguintes campos:

- role: (String) ['admin' ou 'user'] - Obrigatório 
- name: (String) 
- email: (String)
- password (String)
  
`POST /api/users/add` 

 #### Query Users
 
 A Rota pode ser utilizada passando com parametro na url o id do partner (parceiro) que deverá ser encontrado.  
 Ex.: ```http://localhost:8080/api/partner/query?_id=qwe342340934kjjkas```  
 
`GET /api/users/query`  

#### List Users
 
 Essa rota faz uma listagem dos registros de usuarios registrados no sistema, poderá ser passado o parametro valor, que será considerado para filtragem nos campos name e email.  
 Ex.: ```http://localhost:8080/api/users?valor=thiagobianeck@gmail.com```
    
`GET /api/users/list`

 #### Update User  
 
 Rota de atualização dos dados já cadastrados, passar os dados a serem atualizados.
 Obrigatório passar o id do registro a ser atualizado.
 - _id: (String)
- role: (String) ['admin' ou 'user'] 
- name: (String) 
- email: (String)
- password (String)
 
 Ex.: ```http://localhost:8080/api/users/update```
 
`PUT /api/users/update`
 #### Remove User  
 
 Rota para exclusão de usuário.
 Obs.: passar o _id do objeto a ser excluído no body da requisição.  
 
  Ex.: ```http://localhost:8080/api/users/remove```  
  
`DELETE /api/users/remove`

 #### Login  
 
 Rota para login do usuário.
 Obs.: passar o o email e a senha do usuário através do body da requisição.
 
 Após verificação será retornado um token, caso o email e a senha estejam devidamente registradas no sistema.  
 
  Ex.: ```http://localhost:8080/api/users/login```  
  
`DELETE /api/users/login`

#### A grande maioria das funcionalidades desta API poderão ser futuramente melhoradas.


